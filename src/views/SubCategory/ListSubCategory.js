import React, { Component } from 'react';
import { Badge, Card, CardBody, CardHeader, Col, Pagination, PaginationItem, PaginationLink, Row, Table } from 'reactstrap';
import ImageUploader from 'react-images-upload';
let prev  = 0;
let next  = 0;
let last  = 0;
let first = 0;

class Tables extends Component {
  //// declaration de variables
  constructor() {
    super();
    this.state = {
      SubCategory: [],
      currentPage: 1,
      todosPerPage: 5
    };
    this.handleClick = this.handleClick.bind(this);

    this.handleLastClick = this.handleLastClick.bind(this);

    this.handleFirstClick = this.handleFirstClick.bind(this);
  }
  handleClick(event) {

    event.preventDefault();

    this.setState({
      currentPage: Number(event.target.id)
    });
  }



  handleLastClick(event) {

    event.preventDefault();

    this.setState({
      currentPage:last
    });
  }


  handleFirstClick(event) {

    event.preventDefault();

    this.setState({
      currentPage:1
    });
  }


  getAll() {
    fetch("http://localhost:3000/SCategory/SListCat", {method: "GET"})
      .then(response => response.json())
      .then(data => {
        console.log("SubCatagory", data);
        this.setState({SubCategory: data})
      })
  }

  handleClickDelete(e, id){

    e.preventDefault();

    console.log("id",id);
    this.remove(id);
  }
  remove(id)
  {
    fetch("http://localhost:3000/Scategory/deleteCat/"+id, {method: "DELETE"})
      .then(response => response.json())
      .then(data => {
        console.log("remove", data);
        this.getAll();
      })

  }


  handleClickEdit(e, id){
    e.preventDefault();
    console.log("id",id);
    localStorage.setItem("ids",id);
   window.location.href="/#/home/EditSub/"
  }

  componentDidMount() {
    console.log("sous");
    this.getAll();
  }
  render() {
    let {SubCategory, currentPage, todosPerPage} = this.state;


    // Logic for displaying current todos

    let indexOfLastTodo = currentPage * todosPerPage;

    let indexOfFirstTodo = indexOfLastTodo - todosPerPage;

    let currentTodos = SubCategory.slice(indexOfFirstTodo, indexOfLastTodo);


    prev = currentPage > 0 ? (currentPage - 1) : 0;

    last = Math.ceil(SubCategory.length / todosPerPage);

    next = (last === currentPage) ? currentPage : currentPage + 1;



    // Logic for displaying page numbers

    let pageNumbers = [];

    for (let i = 1; i <= last; i++) {
      pageNumbers.push(i);
    }


    console.log("sous");
    return (
      <div className="animated fadeIn">
        <Row>

          <Col xs="12" lg="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> List Sub Category
              </CardHeader>
              <CardBody>
                <Table responsive striped>
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Modifier</th>
                    <th>supp</th>

                  </tr>
                  </thead>
                  <tbody>

                  {

                    currentTodos.map((item,index) =>{

                      return(

                        <tr key={index}>


                        <td>{item.Name}</td>


                        <td><i class='fas fa-edit' style={{color:"green"}} onClick = {evt => this.handleClickEdit(evt,item._id)}></i></td>
                        <td><i class='fas fa-eraser' style={{color:"red"}}
                               onClick = {evt => this.handleClickDelete(evt,item._id)}></i></td>
                      </tr>
                      );

                    })

                  }



                  </tbody>
                </Table>
                <nav>

                  <Pagination>

                    <PaginationItem>
                      { prev === 0 ? <PaginationLink disabled>First</PaginationLink> :
                        <PaginationLink onClick={this.handleFirstClick} id={prev} href={prev}>First</PaginationLink>
                      }
                    </PaginationItem>
                    <PaginationItem>
                      { prev === 0 ? <PaginationLink disabled>Prev</PaginationLink> :
                        <PaginationLink onClick={this.handleClick} id={prev} href={prev}>Prev</PaginationLink>
                      }
                    </PaginationItem>
                    {
                      pageNumbers.map((number,i) =>
                        <Pagination key= {i}>
                          <PaginationItem active = {pageNumbers[currentPage-1] === (number) ? true : false} >
                            <PaginationLink onClick={this.handleClick} href={number} key={number} id={number}>
                              {number}
                            </PaginationLink>
                          </PaginationItem>
                        </Pagination>
                      )}

                    <PaginationItem>
                      {
                        currentPage === last ? <PaginationLink disabled>Next</PaginationLink> :
                          <PaginationLink onClick={this.handleClick} id={pageNumbers[currentPage]} href={pageNumbers[currentPage]}>Next</PaginationLink>
                      }
                    </PaginationItem>

                    <PaginationItem>
                      {
                        currentPage === last ? <PaginationLink disabled>Last</PaginationLink> :
                          <PaginationLink onClick={this.handleLastClick} id={pageNumbers[currentPage]} href={pageNumbers[currentPage]}>Last</PaginationLink>
                      }
                    </PaginationItem>
                  </Pagination>
                </nav>

              </CardBody>
            </Card>
          </Col>
        </Row>


      </div>

    );
  }
}

export default Tables;
