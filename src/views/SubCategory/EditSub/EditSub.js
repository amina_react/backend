import React, { Component } from 'react';
import {Link} from 'react-router-dom';

import {
  Badge,
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButtonDropdown,
  InputGroupText,
  Label,
  Row,
} from 'reactstrap';
import axios from "axios/index";

class Forms extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,
      /*enregistrement de valeur ancien*/
      SubCategory:{
        Name : "",

      },
      Name : "",


    };
  }
  componentDidMount(){
    this.getOne()
  }
getOne(){
  fetch("http://localhost:3000/SCategory/ById/"+localStorage.getItem ("ids"), {method: "GET"})
    .then(response => response.json())
    .then(data => {
      console.log("GETONE", data);
      this.setState({SubCategory:data})

    })
}
  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState }});
  }
  handelSubmit()
{
  console.log("state",this.state.Name);
  if(this.state.Name ==="")
  {
    this.state.Name=this.state.SubCategory.Name
  }

  axios.put("http://localhost:3000/Scategory/Edit/"+localStorage.getItem ("ids"),
    {
    Name:this.state.Name,

})
    .then (res=> {
      console.log ("data",res.data);
      window.location.href="/#/home/SubCategory"
    })
}


  render() {
    return (
      <div className="animated fadeIn">

        <Row>

          <Col xs="12" md="12">

            <Card>
              <CardHeader>
                <strong>Edit Sub Category</strong>
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <FormGroup>
                    <Label htmlFor="nf-email">Name </Label>
                    <Input type="text" id="nf-email" name="nf-email"  autoComplete=""
                     defaultValue={this.state.SubCategory.Name} onChange={event => this.setState({Name: event.target.value})}/>

                    <FormText className="help-block">Please enter Sub category Name </FormText>
                  </FormGroup>

                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" onClick={this.handelSubmit.bind(this)}><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Link to ="/home/SubCategory"> <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Reset</Button></Link>
              </CardFooter>
            </Card>

          </Col>
        </Row>

      </div>
    );
  }
}

export default Forms;
