import React, { Component } from 'react';

import {
  Badge,
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButtonDropdown,
  InputGroupText,
  Label,
  Row,
} from 'reactstrap';
import axios from "axios/index";

class Forms extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,
      Name : "",
      NameEr:"",
      category:[],
      idCat:""

    };
  }
  validate = () => {

    let isError = false;

    const errors = {
      NameEr: "",

    }

    console.log("login ",this.state.Name);




    const regex1=/^[a-zA-Z0-9._-]+$/;


    if ((this.state.Name==="")||(this.state.Name.length > 15)||!regex1.test(this.state.Name)) {

      isError = true;
      errors.EmailErr = "Please enter a Sub Category";
    }

    if (isError) {
      this.setState({
        ...this.state,
        ...errors
      })
    }

    console.log("errrr ", isError)


    this.setState({
      erreur:isError
    })

    return isError;
  }
  componentDidMount() {
    this.getAll();
  }
  getAll() {
    fetch("http://localhost:3000/SCategory/SListCat", {method: "GET"})
      .then(response => response.json())
      .then(data => {
        console.log("SubCategory", data);
        this.setState({SubCategory: data})
      })
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState }});
  }
  handelSubmit()
{
  console.log("state",this.state.Name, this.state.Description);
  axios.post("http://localhost:3000/SCategory/create/",
    {
    Name:this.state.Name,

})
    .then (res=> {
      console.log ("data",res.data);
      window.location.href="/#/home/SubCategory"
    })
}
reset ()
{
this.setState({Name:""})
}

  render() {
    return (
      <div className="animated fadeIn">

        <Row>

          <Col xs="12" md="12">

            <Card>
              <CardHeader>
                <strong>Add Sub Category</strong>
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <FormGroup>
                    <Label htmlFor="ccmonth">Category</Label>
                    <Input type="select" name="ccmonth" id="ccmonth"
                      value={this.state.ids}
                           onChange={evt=>this.setState({idCat:evt.target.value})}>
                      {<option value={"0"}> Choose a Category </option>}
                      {
                        this.state.category.map((item) =>
                          <option value={item._id}>{item.Name}</option>
                        )
                      }

                    </Input>
                  </FormGroup>
                  <FormGroup>
                    <Label htmlFor="nf-email">Name </Label>
                    <Input type="text" id="nf-email" name="nf-email" placeholder="Enter Category Name.." autoComplete=""
                     value={this.state.Name} onChange={event => this.setState({Name: event.target.value})}/>
                    {

                      this.state.erreur===false ?

                        <FormText >{this.state.NameEr}</FormText>:null

                    }
                    {

                      this.state.erreur===true ?

                        <FormText style ={{backgroundColor:"red"}}>{this.state.NameEr}</FormText>:null

                    }

                    <FormText className="help-block">Please category Name </FormText>
                  </FormGroup>

                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" onClick={this.handelSubmit.bind(this)}><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"  onClick={this.reset.bind(this)}><i className="fa fa-ban"></i> Reset</Button>
              </CardFooter>
            </Card>

          </Col>
        </Row>

      </div>
    );
  }
}

export default Forms;
