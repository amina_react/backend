import React, { Component } from 'react';

import {
  Badge,
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButtonDropdown,
  InputGroupText,
  Label,
  Row,
} from 'reactstrap';
import axios from "axios/index";

class Forms extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,
      Name : "",
      Price:"",
      Description:"",
      Image:"",
      file:File,

      SubCategory: [],
      ids:"",
      produit:{
        Name : "",
        Price:"",
        Description:"",
        Image:"",
      }
    };
  }
  handleChangeFile =(evt)=>{
    console.log("file",evt.target.files[0]);
    const file = evt.target.files[0];
    this.setState({file:file})
  };
  componentDidMount() {
    this.getAll();
    this.getOne();
  }
  getAll() {
    fetch("http://localhost:3000/SCategory/SListCat", {method: "GET"})
      .then(response => response.json())
      .then(data => {
        console.log("produit", data);
        this.setState({SubCategory: data})
      })
  }
  getOne(){
    fetch("http://localhost:3000/produit/ByIdP/"+localStorage.getItem ("idp"), {method: "GET"})
      .then(response => response.json())
      .then(data => {
        console.log("GETONE", data);
        this.setState({produit:data})

      })
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState }});
  }
  handelSubmit()
{

  if(this.state.Name ==="")
  {
    this.state.Name=this.state.produit.Name
  }

  if(this.state.Price ==="")
  {
    this.state.Price=this.state.produit.Price
  }

  if(this.state.Description ==="")
  {
    this.state.Description=this.state.produit.Description
  }

  const formdata=new FormData ();
  formdata.append("Name",this.state.Name);
  formdata.append("Description",this.state.Description);
  formdata.append("Price",this.state.Price);
  formdata.append("Image",this.state.file);
  console.log("state",this.state.Name, this.state.Description);
  axios.put("http://localhost:3000/produit/EditP/"+localStorage.getItem ("idp"),formdata)
    .then (res=> {
      console.log ("data",res.data);
      window.location.href="/#/home/produit"
    })
}
reset ()
{
this.setState({Name:"",Price:"",Description:"",Image:""})
}
  render() {
    return (
      <div className="animated fadeIn">

        <Row>

          <Col xs="12" md="12">

            <Card>
              <CardHeader>
                <strong>Edit Produit</strong>
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <FormGroup>
                    <Label htmlFor="ccmonth">Sub Category </Label>
                    <Input type="select" name="ccmonth" id="ccmonth"
                           value={this.state.ids}
                           onChange={evt=>this.setState({ids:evt.target.value})}>
                      {<option value={"0"}> Choose a sub  Category </option>}
                      {
                        this.state.SubCategory.map((item) =>
                          <option value={item._id}>{item.Name}</option>
                        )
                      }
                    </Input>
                  </FormGroup>

                </Form>
                <FormGroup>
                    <Label htmlFor="nf-email">Name </Label>
                    <Input type="text" id="nf-email" name="nf-email"
                     defaultValue={this.state.produit.Name} onChange={event => this.setState({Name: event.target.value})}/>

                    <FormText className="help-block">Please category Name </FormText>
                  </FormGroup>
                <FormGroup>
                  <Label htmlFor="nf-email">Description</Label>
                  <Input type="text" id="nf-email" name="nf-email"
                         defaultValue={this.state.produit.Description} onChange={event => this.setState({Description: event.target.value})}/>

                  <FormText className="help-block">Please category Name </FormText>
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="nf-email">Price</Label>
                  <Input type="text" id="nf-email" name="nf-email" placeholder="Enter Price.." autoComplete=""
                         defaultValue={this.state.produit.Price} onChange={event => this.setState({Price: event.target.value})}/>

                  <FormText className="help-block">Please  Category Price </FormText>
                </FormGroup>
                <FormGroup>

                  <Label htmlFor="nf-email">Photo</Label>
                  <img src={`http://localhost:3000/produit/getFile/${this.state.produit.Image}`}height ="50" width="50"/>
                  <Input type="file" id="nf-email" name="nf-email"
                         onChange={this.handleChangeFile}/>
                  <FormText className="help-block">Please  Category Price </FormText>
                </FormGroup>

              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" onClick={this.handelSubmit.bind(this)}><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"  onClick={this.reset.bind(this)}><i className="fa fa-ban"></i> Reset</Button>
              </CardFooter>
            </Card>

          </Col>
        </Row>

      </div>
    );
  }
}

export default Forms;
