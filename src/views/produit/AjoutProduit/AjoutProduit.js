import React, { Component } from 'react';

import {
  Badge,
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButtonDropdown,
  InputGroupText,
  Label,
  Row,
} from 'reactstrap';
import axios from "axios/index";

class Forms extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,
      Name : "",
      NameERR : "",
      Price:"",
      PriceERR:"",
      Description:"",
      DescriptionERR:"",
      Image:"",
      ImageERR:"",
      file:File,

      SubCategory: [],
      ids:"",
      SubCategoryERR:"",


    };
  }
  validate = () => {

    let isError = false;

    const errors = {
      NameERR: "",
      PriceERR: "",
      DescriptionERR: "",
      ImageERR: "",
      SubCategoryERR: "",
    }

    const regex1=/^[a-zA-Z0-9._-]+$/;
    if (this.state.ids==="") {

      isError = true;
      errors.SubCategoryERR = "Please Select Category Name";
    }

    if ((this.state.Name==="")||(this.state.Name.length > 15)||!regex1.test(this.state.Name)) {

      isError = true;
      errors.NameERR = "Please Enter product Name";
    }


    if ((this.state.Price==="")||(this.state.Price.length > 20)) {

      isError = true;
      errors.PriceERR = "Please add the price ";
    }
    if ((this.state.Description==="")||(this.state.Description.length > 20)) {

      isError = true;
      errors.DescriptionERR = "Please add the Description ";
    }




    if (isError) {
      this.setState({
        ...this.state,
        ...errors
      })
    }

    console.log("errrr ", isError)


    this.setState({
      erreur:isError
    })

    return isError;
  }
  handleChangeFile =(evt)=>{
    console.log("file",evt.target.files[0]);
    const file = evt.target.files[0];
    this.setState({file:file})
  };
  componentDidMount() {
    this.getAll();
  }
  getAll() {
    fetch("http://localhost:3000/category/ListCat", {method: "GET"})
      .then(response => response.json())
      .then(data => {
        console.log("produit", data);
        this.setState({SubCategory: data})
      })
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState }});
  }
  async handelSubmit()
{
  const formdata=new FormData ();

  formdata.append("Name",this.state.Name);
  formdata.append("Description",this.state.Description);
  formdata.append("Price",this.state.Price);
  formdata.append("Image",this.state.file);

  let err=this.validate();
  if(!err){
  console.log("state",this.state.Name, this.state.Description);
  await axios.post("http://localhost:3000/produit/Add",formdata)
    .then (res=> {
      console.log ("data",res.data);
      axios.put("http://localhost:3000/category/push/"+this.state.ids,{
        Produit:res.data['_id']
      })
        .then (res=> {
          console.log ("data",res.data);
         // window.location.href="/#/home/produit"
        })
     window.location.href="/#/home/produit"
    })
}}
reset ()
{
this.setState({Name:"",Price:"",Description:"",Image:""})
}

  render() {
    return (
      <div className="animated fadeIn">

        <Row>

          <Col xs="12" md="12">

            <Card>
              <CardHeader>
                <strong>Add Produit</strong>
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <FormGroup>
                    <Label htmlFor="ccmonth">Sub Category </Label>
                    <Input type="select" name="ccmonth" id="ccmonth"
                           value={this.state.ids}
                           onChange={evt=>this.setState({ids:evt.target.value})}>
                      {<option value={"0"}> Choose a sub  Category </option>}
                      {
                        this.state.SubCategory.map((item) =>
                          <option value={item._id}>{item.Name}</option>
                        )
                      }

                    </Input>
                    {
                      this.state.erreur===false ?

                        <FormText >{this.state.SubCategoryERR}</FormText>:null

                    }
                    {
                      this.state.erreur===true ?
                        <FormText style ={{backgroundColor:'red'}}>{this.state.SubCategoryERR}</FormText>:null
                    }

                  </FormGroup>


                </Form>
                <FormGroup>
                    <Label htmlFor="nf-email">Name </Label>
                    <Input type="text" id="nf-email" name="nf-email"
                     value={this.state.Name} onChange={event => this.setState({Name: event.target.value})}/>
                  {

                    this.state.erreur===false ?

                      <FormText >{this.state.NameERR}</FormText>:null

                  }
                  {

                    this.state.erreur===true ?

                      <FormText style ={{backgroundColor:"red"}}>{this.state.NameERR}</FormText>:null

                  }


                  </FormGroup>
                <FormGroup>
                  <Label htmlFor="nf-email">Description</Label>
                  <Input type="text" id="nf-email" name="nf-email"
                         value={this.state.Description} onChange={event => this.setState({Description: event.target.value})}/>
                  {

                    this.state.erreur===false ?

                      <FormText >{this.state.DescriptionERR}</FormText>:null

                  }
                  {

                    this.state.erreur===true ?

                      <FormText style ={{backgroundColor:'red'}}>{this.state.DescriptionERR}</FormText>:null

                  }


                </FormGroup>
                <FormGroup>
                  <Label htmlFor="nf-email">Price</Label>
                  <Input type="Number" id="nf-email" name="nf-email"
                         value={this.state.Price} onChange={event => this.setState({Price: event.target.value})}/>
                  {

                    this.state.erreur===false ?

                      <FormText >{this.state.PriceERR}</FormText>:null

                  }
                  {

                    this.state.erreur===true ?

                      <FormText style ={{backgroundColor:'red'}}>{this.state.PriceERR}</FormText>:null

                  }

                </FormGroup>
                <FormGroup>
                  <Label>Photo</Label>
                  <Input type="file" onChange={this.handleChangeFile}/>
                  {

                    this.state.erreur===false ?

                      <FormText >{this.state.ImageERR}</FormText>:null

                  }
                  {

                    this.state.erreur===true ?

                      <FormText style ={{backgroundColor:'red'}}>{this.state.ImageERR}</FormText>:null

                  }


                </FormGroup>

              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" onClick={this.handelSubmit.bind(this)}><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"  onClick={this.reset.bind(this)}><i className="fa fa-ban"></i> Reset</Button>
              </CardFooter>
            </Card>

          </Col>
        </Row>

      </div>
    );
  }
}

export default Forms;
