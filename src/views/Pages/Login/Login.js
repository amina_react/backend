import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { FormText,Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import axios from 'axios';

class Login extends Component {
  constructor() {
    super();
    this.state = {
    Email: "",
    Password: "",
    EmailErr: "",
    PasswordErr: "",
    erreur:false
    }

  }
  login() {
    let err = this.validate();
    if (!err) {
      console.log("state ", this.state);
      axios.post("http://localhost:3000/user/authenticate", {
        Email: this.state.Email,
        Password: this.state.Password
      })


        .then(res => {
          console.log("respose", res.data);
          if (res.data['status'] === "error") {
            alert(" verifier votre login ou password")
          }
          else {
            alert("Done ! ");
            window.location.href = "/#/home/category"
          }

        })
    }
  }
  validate = () => {

    let isError = false;

    const errors = {
      EmailErr: "",
      PasswordErr: "",
    }

    console.log("login ",this.state.Email);
    console.log("pws ",this.state.Password);



    const regex1=/^[a-zA-Z0-9._-]+$/;


    if ((this.state.Email==="")||(this.state.Email.length > 15)||!regex1.test(this.state.Email)) {

      isError = true;
      errors.EmailErr = "Veuillez verifier votre Email";
    }


    if ((this.state.Password==="")||(this.state.Password.length > 20)) {

      isError = true;
      errors.PasswordErr = "veuillez verifier votre mot de passe";
    }



    if (isError) {
      this.setState({
        ...this.state,
        ...errors
      })
    }

    console.log("errrr ", isError)


    this.setState({
      erreur:isError
    })

    return isError;
  }

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form>
                      <h1>Connexion</h1><p className="text-muted">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>

                        <Input type="text" placeholder="Email" autoComplete="username" value={this.state.Email}
                               onChange={evt => this.setState({Email: evt.target.value})}/>

                        {

                          this.state.erreur===false ?

                            <FormText >{this.state.EmailErr}</FormText>:null

                        }
                        {

                          this.state.erreur===true ?

                            <FormText style ={{backgroundColor:"red"}}>{this.state.EmailErr}</FormText>:null

                        }



                      </InputGroup>

                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="Password" placeholder="Password"  autoComplete="username" value={this.state.Password}
                               onChange={event => this.setState({Password: event.target.value})}/>
                        {

                          this.state.erreur===false ?

                            <FormText >{this.state.PasswordErr}</FormText>:null

                        }
                        {

                          this.state.erreur===true ?

                            <FormText style ={{backgroundColor:"red"}}>{this.state.PasswordErr}</FormText>:null

                        }
                      </InputGroup>

                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4"
                                  onClick={this.login.bind(this)}> Connecter </Button>
                        </Col>

                      </Row>
                    </Form>
                  </CardBody>
                </Card>

              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
