import React, { Component } from 'react';
import {

  Badge,
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButtonDropdown,
  InputGroupText,
  Label,
  Row,
} from 'reactstrap';
import axios from "axios/index";

class Forms extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,
      Name : "",
      Description : "",
      NameErr: "",
      DescriptionErr: "",
      erreur:false
    };
  }
  validate = () => {

    let isError = false;

    const errors = {
      NameErr: "",
      DescriptionErr: "",
    }

    console.log("login ",this.state.Name);
    console.log("pws ",this.state.Description);



    const regex1=/^[a-zA-Z0-9._-]+$/;


    if ((this.state.Name==="")||(this.state.Name.length > 15)||!regex1.test(this.state.Name)) {

      isError = true;
      errors.NameErr = "Veuillez verifier votre Nom";
    }


    if ((this.state.Description==="")||(this.state.Description.length > 20)) {

      isError = true;
      errors.DescriptionErr = "veuillez verifier votre Description";
    }



    if (isError) {
      this.setState({
        ...this.state,
        ...errors
      })
    }

    console.log("errrr ", isError)


    this.setState({
      erreur:isError
    })

    return isError;
  }

  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState }});
  }
  handelSubmit()
{
  let err=this.validate();
  if(!err){
    console.log("state",this.state.Name, this.state.Description);
    axios.post("http://localhost:3000/category/create",{
      Name:this.state.Name,
      Description:this.state.Description
    })
      .then (res=> {
        console.log ("data",res.data);
        window.location.href="/#/home/category"
      })
  }

}
reset ()
{
this.setState({Name:"" , Description:""})
}

  render() {
    return (
      <div className="animated fadeIn">

        <Row>

          <Col xs="12" md="12">

            <Card>
              <CardHeader>
                <strong>Add Category</strong>
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <FormGroup>
                    <Label htmlFor="nf-email">Name </Label>
                    <Input type="text" id="nf-email" name="nf-email" autoComplete=""
                     value={this.state.Name} onChange={evt => this.setState({Name: evt.target.value})}/>
                    {

                      this.state.erreur===false ?

                        <FormText >{this.state.NameErr}</FormText>:null

                    }
                    {

                      this.state.erreur===true ?

                        <FormText style ={{backgroundColor:'red'}}>{this.state.NameErr}</FormText>:null

                    }


                  </FormGroup>
                  <FormGroup>
                    <Label htmlFor="nf-password">Description</Label>
                    <Input type="text" id="nf-password" name="nf-password"  autoComplete=""
                           value={this.state.Description} onChange={event => this.setState({Description: event.target.value})}/>
                    {

                      this.state.erreur===false ?

                        <FormText >{this.state.DescriptionErr}</FormText>:null

                    }
                    {

                      this.state.erreur===true ?

                        <FormText style ={{backgroundColor:'red'}}>{this.state.DescriptionErr}</FormText>:null

                    }


                  </FormGroup>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" onClick={this.handelSubmit.bind(this)}><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Button type="reset" size="sm" color="danger"  onClick={this.reset.bind(this)}><i className="fa fa-ban"></i> Reset</Button>
              </CardFooter>
            </Card>

          </Col>
        </Row>

      </div>
    );
  }
}

export default Forms;
