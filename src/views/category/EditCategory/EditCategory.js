import React, { Component } from 'react';
import {Link} from 'react-router-dom';

import {
  Badge,
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Collapse,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Fade,
  Form,
  FormGroup,
  FormText,
  FormFeedback,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupButtonDropdown,
  InputGroupText,
  Label,
  Row,
} from 'reactstrap';
import axios from "axios/index";

class Forms extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleFade = this.toggleFade.bind(this);
    this.state = {
      collapse: true,
      fadeIn: true,
      timeout: 300,
      /*enregistrement de valeur ancien*/
      category:{
        Name : "",
        Description : ""
      },
      Name : "",
      Description : ""

    };
  }
  componentDidMount(){
    this.getOne()
  }
  validate = () => {

    let isError = false;

    const errors = {
      NameErr: "",
      DescriptionErr: "",
    }

    console.log("login ",this.state.Name);
    console.log("pws ",this.state.Description);



    const regex1=/^[a-zA-Z0-9]+$/;


    if ((this.state.Name !="")&&(this.state.Name.length > 15)||!regex1.test(this.state.Name)) {

      isError = true;
      errors.NameErr = "Please enter the category Name";
    }


    if ((this.state.Description==="")||(this.state.Description.length > 20)) {

      isError = true;
      errors.DescriptionErr = "Please enter your description ";
    }



    if (isError) {
      this.setState({
        ...this.state,
        ...errors
      })
    }

    console.log("errrr ", isError)


    this.setState({
      erreur:isError
    })

    return isError;
  }
getOne(){
  fetch("http://localhost:3000/category/ById/"+localStorage.getItem ("idc"), {method: "GET"})
    .then(response => response.json())
    .then(data => {
      console.log("GETONE", data);
      this.setState({category:data})

    })
}
  toggle() {
    this.setState({ collapse: !this.state.collapse });
  }

  toggleFade() {
    this.setState((prevState) => { return { fadeIn: !prevState }});
  }
  handelSubmit() {
    let err = this.validate();
    if (!err) {
      console.log("state", this.state.Name, this.state.Description);
      if (this.state.Name === "") {
        this.state.Name = this.state.category.Name
      }
      if (this.state.Description === "") {
        this.state.Description = this.state.category.Description
      }
      axios.put("http://localhost:3000/category/EditC/" + localStorage.getItem("idc"),
        {
          Name: this.state.Name,
          Description: this.state.Description,
        })
        .then(res => {
          console.log("data", res.data);
          window.location.href = "/#/home/category"
        })
    }
  }


  render() {
    return (
      <div className="animated fadeIn">

        <Row>

          <Col xs="12" md="12">

            <Card>
              <CardHeader>
                <strong>Edit Category</strong>
              </CardHeader>
              <CardBody>
                <Form action="" method="post">
                  <FormGroup>
                    <Label htmlFor="nf-email">Name </Label>
                    <Input type="text" id="nf-email" name="nf-email"  autoComplete=""
                     defaultValue={this.state.category.Name} onChange={event => this.setState({Name: event.target.value})}/>
                    {

                      this.state.erreur===false ?

                        <FormText >{this.state.NameErr}</FormText>:null

                    }
                    {

                      this.state.erreur===true ?

                        <FormText style ={{backgroundColor:"red"}}>{this.state.NameErr}</FormText>:null
                    }
                  </FormGroup>
                  <FormGroup>
                    <Label htmlFor="nf-password">Description</Label>
                    <Input type="text" id="nf-password" name="nf-password"  autoComplete=""
                           defaultValue={this.state.category.Description} onChange={event => this.setState({Description: event.target.value})}/>
                    {

                      this.state.erreur===false ?

                        <FormText >{this.state.DescriptionErr}</FormText>:null

                    }
                    {

                      this.state.erreur===true ?

                        <FormText style ={{backgroundColor:"red"}}>{this.state.DescriptionErr}</FormText>:null
                    }
                    <FormText className="help-block">Please enter your Category Description </FormText>
                  </FormGroup>
                </Form>
              </CardBody>
              <CardFooter>
                <Button type="submit" size="sm" color="primary" onClick={this.handelSubmit.bind(this)}><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <Link to ="/home/category"> <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Reset</Button></Link>
              </CardFooter>
            </Card>

          </Col>
        </Row>

      </div>
    );
  }
}

export default Forms;
