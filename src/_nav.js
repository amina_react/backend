export default {
  items: [
    {
      name: 'Category',
      url: '/home/category',
      icon: 'icon-speedometer',
     children :[
       {
         name: 'ListCategory',
         url: '/home/category',
         icon: 'cui-file',
       },
       {
         name: 'AjoutCategory',
         url: '/home/AjoutCategory',
         icon: 'cui-file',
       }

     ]


    },
    {
      name: 'Sub Category',
      url: '/home/SubCategory',
      icon: 'icon-speedometer',
      children :[
        {
          name: 'ListSubCategory',
          url: '/home/SubCategory',
          icon: 'cui-file',
        },
        {
          name: 'Add Sub Category',
          url: '/home/AjoutSub',
          icon: 'icon-speedometer',
        }

      ]
    },

    {
      name: 'Produit',
      url: '/home/produit',
      icon: 'icon-speedometer',
      children :[
        {
          name: 'List Produit',
          url: '/home/produit',
          icon: 'cui-file',
        },
        {
          name: 'Ajout Produit',
          url: '/home/AjoutProduit',
          icon: 'cui-file',
        }
        ]
    },


  ],

};
