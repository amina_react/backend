var mongoose = require('mongoose');
const User = require('./userModel');

const Client = User.discriminator('Client', new mongoose.Schema({
  nom : {
    type: String ,
    required:true,
    trim:true /* pour les espaces */
  },
  Prénom : {
    type: String ,
    required:true,
    trim:true

  },
  Email : {
    type: String ,
    required:true,
    trim:true

  },
  Password : {
    type: String ,
    required:true

  },
  Adresse: {
      type: String,
      required: true
    },
  })
);


module.exports = Client;
