var mongoose = require('mongoose');
//cryptage//

const bcrypt = require('bcrypt');
const saltRounds = 10;
const jwt = require('jsonwebtoken');
const Options ={
  discriminatorKey:'Type_user',// our discrimination key could be anything
  collection:'user', // the name of our collection
};

const UserSchema = mongoose.model('User',new mongoose.Schema (
  {

    nom: {
      type: String,
      required: true,
      trim: true /* pour les espaces */
    },
    Prénom: {
      type: String,
      required: true,
      trim: true

    },
    Email: {
      type: String,
      required: true,
      trim: true

    },
    Password: {
      type: String,
      required: true

    },
    Avatar: {

      type: String,
      required: true,

    },
  }

  ,Options
)
  .pre("save",function (next) {
    this.Password = bcrypt.hashSync(this.Password, 10);
    next();

  }
  ));


  module.exports = UserSchema;


