var mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategorySchema = mongoose.model('Category',new mongoose.Schema ({

    Name: {
      type: String ,
      required:true,
      trim:true /* pour les espaces */
    },
    Description : {
      type: String ,
      required:true,
      trim:true

    },
  SCategoryM: [
    {
      type: Schema.Types.ObjectId, ref: "SCategoryM"
    }
  ],

  Produit: [
    {
      type: Schema.Types.ObjectId, ref: "Produit"
    }
  ]


  })
);
module.exports = CategorySchema;
