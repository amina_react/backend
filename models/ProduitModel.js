var mongoose = require('mongoose');



const ProduitSchema = mongoose.model('Produit',new mongoose.Schema ({

    Name: {
      type: String ,
      required:true,
      trim:true /* pour les espaces */
    },
    Price : {
      type: Number ,
      required:true,

    },
    Description : {
      type: String ,
      required:true,
      trim:true

    },
  Image : {
    type: String ,
    required:true,

  },
  })
);
module.exports = ProduitSchema;
