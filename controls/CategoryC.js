const CategoryM= require('../models/categoryM');




module.exports= {


  create: function (req, res) {
    const Category = new CategoryM({

      Name: req.body.Name,
      Description: req.body.Description,
      SCategoryM: req.body.SCategoryM,
    });

    Category.save(function (err) {
      if (err) {
        res.json({state: 'no', msg: 'Warning!!! Error!!!' + err})

      }
      else {
        res.json({state: 'ok ', msg: 'You did it :) Category added'})


      }


    })

  },


  ListCat: function (req, res) {
    CategoryM.find({},function (err, ListCat) {

        if (err) {

          res.json({state: ' no ', msg: 'Category not found'})

        }
        else {
          res.json(ListCat)

        }


      }
    )

  },


  ById: function (req, res) {
    CategoryM.findOne({_id: req.params.id}).populate({ path: 'SCategoryM', populate: { path: 'Produit' }}).exec(function (err, Category){

        if (err) {

          res.json({state: ' no ', msg: 'ID not found'})

        }
        else {
          res.json(Category)

        }


      }
    )

  },


  deleteCat : function (req , res) {

    CategoryM.findOneAndRemove({_id: req.params.id}, function (err){

        if (err) {

          res.json({state: ' no ', msg: 'Erreur ID not Found '})

        }
        else {
          res.json({ msg: 'Done ! Category was Deleted'})

        }


      }
    )

  },

  EditC: function (req, res) {
    CategoryM.updateOne({_id: req.params.id},
      {
        Name: req.body.Name,
        Description: req.body.Description

      }

      , function (err) {

        if (err) {
          res.json({state: ' no ', msg: 'ID not found'})

        }

        else {
          res.json({state: 'ok', msg: 'Category updated'})


        }
      })


  },
  Push: function (req, res) {
    CategoryM.updateOne({_id: req.params.id},{$push:{Produit: req.body.Produit}}, function (err, data) {

      if (err) {
        res.json({state: ' no ', msg: 'ID not found'})

      }

      else {
        res.json(data)


      }
    })


  },

};
