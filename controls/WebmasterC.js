const Webmaster = require('../models/Webmaster');
/*to upload file : add multer package with destination path*/
const multer = require ('multer');
const upload = multer({ dest: __dirname +'/uploads/images' });
var fs= require("fs");
module.exports= {
  Add: function (req, res) {
    var file = __dirname + '/uploads/' + req.file.originalname;
    fs.readFile(req.file.path, function (err, data) {
      fs.writeFile(file, data, function (err) {
        if (err) {

          console.error(err);
          var response = {
            message: 'sorry file couldnt upload',
            filename: req.file.originalname
          };
        }
        else {
          const webmaster = new Webmaster({
            nom: req.body.nom,
            Prénom: req.body.Prénom,
            Email: req.body.Email,
            Password: req.body.Password,
            Adresse: req.body.Adresse,
            Avatar: req.file.originalname
          });
          webmaster.save(function (err) {
            if (err) {


              res.json({state: 'no', msg: 'erreur' + err})

            }
            else {

              res.json({state: 'ok ', msg: 'Webmaster added'})
            }
          })
        }
      })
    })
  },
  ListWebmaster: function (req, res) {
    Webmaster.find({}, function (err, ListWebmaster) {

        if (err) {

          res.json({state: ' no ', msg: 'Webmaster Not Found'})

        }
        else {
          res.json(ListWebmaster)

        }


      }
    )

  },
  ById: function (req, res) {
    Webmaster.findOne({_id: req.params.id}, function (err, Webmaster) {

        if (err) {

          res.json({state: ' no ', msg: 'Webmaster ID not found'})

        }
        else {
          res.json(Webmaster)

        }


      }
    )

  },
  deleteWebmaster: function (req, res) {

    Webmaster.findOneAndRemove({_id: req.params.id}, function (err) {

        if (err) {

          res.json({state: ' no ', msg: 'Erreur ID not Found '})

        }
        else {
          res.json({msg: 'Done ! Webmaster was Deleted'})

        }


      }
    )

  },
  Upload: function (req, res) {
    var file = __dirname + '/uploads/' + req.file.originalname;
    fs.readFile(req.file.path, function (err, data) {
      fs.writeFile(file, data, function (err) {
        if (err) {

          console.error(err);
          var response = {
            message: 'sorry file couldnt upload',
            filename: req.file.originalname
          };
        }
        else {
          res.json({state: 'ok '})


        }

      })
    })
  },
  getFile: function (req, res) {
    res.sendFile(__dirname + '/uploads/' + req.params.Avatar)

  }
};
