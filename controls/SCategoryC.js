const SCategoryM= require('../models/SCategoryM');




module.exports= {


  create: function (req, res) {
    const SCategory = new SCategoryM({

      Name: req.body.Name,
      Produit: req.body.Produit,

    });

    SCategory.save(function (err) {
      if (err) {
        res.json({state: 'no', msg: 'Warning!!! Error!!!' + err})

      }
      else {
        res.json({state: 'ok ', msg: 'You did it :) Child Category added'})

      }

    })

  },


  ListCat: function (req, res) {
    SCategoryM.find({},function (err, ListCate) {

          if (err) {

            res.json({state: ' no ', msg: 'Category not found'})

          }
          else {
            res.json(ListCate)

          }


        }
      )

  },

  ById: function (req, res) {
    SCategoryM.findOne({_id: req.params.id}).populate('Produit').exec(function (err, SCategory){

        if (err) {

          res.json({state: ' no ', msg: 'ID not found'})

        }
        else {
          res.json(SCategory)

        }

      }
    )

  },

  deleteCat : function (req , res) {

    SCategoryM.findOneAndRemove({_id: req.params.id}, function (err){

        if (err) {

          res.json({state: ' no ', msg: 'Erreur ID not Found '})

        }
        else {
          res.json({ msg: 'Done ! Child Category was Deleted'})

        }


      }
    )

  },

  Push: function (req, res) {
    SCategoryM.updateOne({_id: req.params.id},{$push:{Produit: req.body.Produit}}, function (err) {

      if (err) {
        res.json({state: ' no ', msg: 'ID not found'})

      }

      else {
        res.json({state: 'ok', msg: 'child category updated'})


      }
    })


  },

  Edit: function (req, res) {
    SCategoryM.updateOne({_id: req.params.id},
      {
        Name: req.body.Name,

       }, function (err) {

      if (err) {
        res.json({state: ' no ', msg: 'ID not found'})

      }

      else {
        res.json({state: 'ok', msg: 'child category updated'})


      }
    })


  }



};
