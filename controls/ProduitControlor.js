const ProduitModel = require('../models/ProduitModel');
/*to upload file : add multer package with destination path*/
const multer = require ('multer');
const upload = multer({ dest: __dirname +'/uploads/images' });
var fs= require("fs");
module.exports= {
  Add: function (req, res) {
    var file = __dirname + '/uploads/' + req.file.originalname;
    fs.readFile(req.file.path, function (err, data) {
      fs.writeFile(file, data, function (err) {
        if (err) {

          console.error(err);
          var response = {
            message: 'sorry file could not upload',
            filename: req.file.originalname
          };
        }
        else {
          const produit = new ProduitModel({
            Name: req.body.Name,
            Price: req.body.Price,
            Description: req.body.Description,
            Image: req.file.originalname
          });
          produit.save(function (err , data) {
            if (err) {


              res.json({state: 'no', msg: 'erreur' + err})

            }
            else {

              res.json(data)
            }
          })
        }
      })
    })
  },
  getFile: function (req, res) {
    res.sendFile(__dirname + '/uploads/' + req.params.Image)

  },
  ListProduit: function (req, res) {
    ProduitModel.find({}, (function (err, ListProduit) {

          if (err) {

            res.json({state: ' no ', msg: 'Product not found'})

          }
          else {
            res.json(ListProduit)

          }


        }
      )
    )
  },
  ByIdP: function (req, res) {
    ProduitModel.findOne({_id: req.params.id}, function (err, Produit) {

        if (err) {

          res.json({state: ' no ', msg: 'Product ID not found'})

        }
        else {
          res.json(Produit)

        }


      }
    )

  },

  deleteProduit: function (req, res) {

    ProduitModel.findOneAndRemove({_id: req.params.id}, function (err) {

        if (err) {

          res.json({state: ' no ', msg: 'Erreur Product ID not Found '})

        }
        else {
          res.json({msg: 'Done ! Product was Deleted'})

        }


      }
    )

  },
  EditP: function (req, res) {
    ProduitModel.updateOne({_id: req.params.id},
      {
        Name: req.body.Name,
        Price: req.body.Price,
        Description: req.body.Description,
        Image: req.body.Image
      }

      , function (err, data) {

        if (err) {

          res.json({state: 'no', msg: 'product not found' + err})
        }
        else {
          res.json({state: 'okk', msg: 'product updated' +data})
        }

      })
  },


  Upload: function (req, res) {
    var file = __dirname + '/uploads/' + req.file.originalname;
    fs.readFile(req.file.path, function (err, data) {
      fs.writeFile(file, data, function (err) {
        if (err) {

          console.error(err);
          var response = {
            message: 'sorry file couldnt upload',
            filename: req.file.originalname
          };
        }
        else {
          res.json({state: 'ok '})


        }

      })
    })
  },





}
