const clientModel = require('../models/clientModel');
module.exports= {

  Add: function (req, res) {
    const client = new clientModel({

      nom: req.body.nom,
      Prénom: req.body.Prénom,
      Email: req.body.Email,
      Password: req.body.Password,
      Adresse:req.body.Adresse,

    });

    client.save(function (err) {
      if (err) {


        res.json({state: 'no', msg: 'erreur' + err})

      }
      else {

        res.json({state: 'ok ', msg: 'Client added'})


      }


    })


  },

  ListClient: function (req, res) {
    clientModel.find({},function (err, ListClient) {

        if (err) {

          res.json({state: ' no ', msg: 'Client Not Found'})

        }
        else {
          res.json(ListClient)

        }


      }
    )

  },


  ById: function (req, res) {
    clientModel.findOne({_id: req.params.id}, function (err, Client){

        if (err) {

          res.json({state: ' no ', msg: 'client ID not found'})

        }
        else {
          res.json(Client)

        }


      }
    )

  },


  deleteClient : function (req , res) {

    clientModel.findOneAndRemove({_id: req.params.id}, function (err){

        if (err) {

          res.json({state: ' no ', msg: 'Erreur ID not Found '})

        }
        else {
          res.json({ msg: 'Done ! Client was Deleted'})

        }


      }
    )

  }





};
