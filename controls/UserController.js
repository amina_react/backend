const UserModel = require('../models/userModel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

/*to upload file : add multer package with destination path*/
const multer = require ('multer');
const upload = multer({ dest: __dirname +'/uploads/images' });
var fs= require("fs");

module.exports= {

  create: function (req, res) {
    var file = __dirname +'/uploads/'+ req.file.originalname;
    fs.readFile(req.file.path ,function(err,data)
    {
      fs.writeFile(file,data , function(err) {
        if (err) {

          console.error(err);
          var response = {
            message: 'sorry file couldnt upload',
            filename: req.file.originalname
          };
        }
        else {
          const User = new UserModel({
            nom: req.body.nom,
            Prénom: req.body.Prénom,
            Email: req.body.Email,
            Password: req.body.Password,
            Adresse:req.body.Adresse,
            Avatar: req.file.originalname,

          });
          User.save(function (err) {
            if (err) {


              res.json({state: 'no', msg: 'erreur' + err})

            }
            else {

              res.json({state: 'ok ', msg: 'User added'})
            }
          })
        }
      })
    })
  },



  ListUser: function (req, res) {
    UserModel.find({}, (function (err, Listuser) {

          if (err) {

            res.json({state: ' no ', msg: 'user not found'})

          }
          else {
            res.json(Listuser)

          }


        }
      )
    )
  },



  ById: function (req, res) {
    UserModel.findOne({_id: req.params.id}, function (err, user){

          if (err) {

            res.json({state: ' no ', msg: 'ID not found'})

          }
          else {
            res.json(user)

          }


        }
    )

  },



deleteUser : function (req , res) {

    UserModel.findOneAndRemove({_id: req.params.id}, function (err){

    if (err) {

      res.json({state: ' no ', msg: 'Erreur ID not Found '})

    }
    else {
      res.json({ msg: 'Done ! user was Deleted'})

    }


  }
)

},


authenticate: function(req, res, next) {
    UserModel.findOne({Email:req.body.Email}, function(err, userInfo){
      if (err) {
        next(err);
      }

      else
        if (userInfo !=null) {
          if (bcrypt.compareSync(req.body.Password, userInfo.Password)) {
            const token = jwt.sign({id: userInfo._id}, req.app.get('secretKey'), {expiresIn: '1h'});
            res.json({status: "success", message: "user found!!!", data: {user: userInfo, token: token}});
          } else {
            res.json({status: "error", message: "Invalid email/password!!!", data: null});
          }
        }

      else{
        res.json({status:"error", message: "null ", data:null});


      }

    });
  },



};

