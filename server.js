/* importer le package express*/
const express = require('express');
const bodyParser = require('body-parser');
var cors = require('cors');




//router privée
const userRouter= require ('./roots/UserRouter');
const clientRouter= require ('./roots/ClientRouter');
const WebmasterC= require ('./roots/WebmasterR');
const ProduitRouter= require ('./roots/ProduitRouter');
const CategoryR= require ('./roots/CategoryR');
const SCategoryR= require ('./roots/SCategoryR');



const DB = require ('./models/DB');

const app = express();


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());



app.use(cors());

app.set('secretKey', 'test');

app.use('/user',userRouter);
app.use('/client',clientRouter);
app.use('/webmaster',WebmasterC);
app.use('/produit',ProduitRouter);
app.use('/Category',CategoryR);
app.use('/SCategory',SCategoryR);



app.listen(3000, function(){



console.log("welcome")
});
