const clientController = require('../controls/ClientConroller');
const router = require('express').Router();

router.post('/Add', clientController.Add);
router.get('/ListUser', clientController.ListClient);
router.get('/ById/:id', clientController.ById);
router.delete('/deleteUser/:id', clientController.deleteClient);

module.exports=router;
