const WebmasterC = require('../controls/WebmasterC');
const multer = require ('multer');

const upload = multer({ dest:__dirname +'uploads/Image' });
const router = require('express').Router();
router.post('/Add',upload.single('Avatar'), WebmasterC.Add);
router.post('/Upload',upload.single('Avatar'), WebmasterC.Upload);
router.get('/ListUser', WebmasterC.ListWebmaster);
router.get('/ById/:id', WebmasterC.ById);
router.delete('/deleteUser/:id', WebmasterC.deleteWebmaster);
router.get('/getFile/:Avatar',WebmasterC.getFile);



module.exports=router;
