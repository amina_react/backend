const UserController = require('../controls/UserController');

const multer = require ('multer');
const upload = multer({ dest:__dirname +'uploads/Image' });

const router = require('express').Router();

router.post('/create',upload.single('Avatar'),UserController.create);
router.post('/authenticate', UserController.authenticate);
router.get('/ListUser', UserController.ListUser);
router.get('/ById/:id', UserController.ById);
router.delete('/deleteUser/:id', UserController.deleteUser);



module.exports=router;
