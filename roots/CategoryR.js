const CategoryC = require('../controls/CategoryC');


const router = require('express').Router();

router.post('/create', CategoryC.create);
router.get('/ListCat', CategoryC.ListCat);
router.get('/ById/:id', CategoryC.ById);
router.delete('/deleteCat/:id', CategoryC.deleteCat);
router.put('/EditC/:id', CategoryC.EditC);
router.put('/Push/:id',CategoryC.Push);




module.exports=router;
