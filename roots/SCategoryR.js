const SCategoryC = require('../controls/SCategoryC');


const router = require('express').Router();

router.post('/create', SCategoryC.create);
router.get('/SListCat', SCategoryC.ListCat);
router.get('/ById/:id', SCategoryC.ById);
router.delete('/deleteCat/:id', SCategoryC.deleteCat);
router.put('/Push/:id', SCategoryC.Push);
router.put('/Edit/:id', SCategoryC.Edit);

module.exports=router;
