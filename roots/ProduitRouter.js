const ProduitController = require('../controls/ProduitControlor');

const multer = require ('multer');

const upload = multer({ dest:__dirname +'uploads/Image' });


const router = require('express').Router();

router.post('/Add',upload.single('Image'), ProduitController.Add);
router.post('/Upload',upload.single('Image'), ProduitController.Upload);
router.get('/ListProduit', ProduitController.ListProduit);
router.get('/ByIdP/:id', ProduitController.ByIdP);
router.delete('/deleteProduit/:id', ProduitController.deleteProduit);
router.put('/EditP/:id',upload.single('Image'),ProduitController.EditP);
router.get('/getFile/:Image',ProduitController.getFile);

module.exports=router;
